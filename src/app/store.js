import { createStore, combineReducers, compose, applyMiddleware } from 'redux';

import filterReducer from './reducers/filter';
import productsReducer from './reducers/products';
import basketReducer from './reducers/basket';

import thunk from 'redux-thunk';

const rootReducer = combineReducers({
	filterReducer,
	productsReducer,
	basketReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));


export default store