const initialState = {
	items: {},
	totalPrice: 0,
	totalCount: 0,
}

const basket = (state = initialState, action) => {
	if (action.type === 'ADD_PRODUCT_BASKET') {
		const newItems = {
		...state.items,
				[action.payload.id]: !state.items[action.payload.id]
				? [action.payload]
				: [...state.items[action.payload.id], action.payload]
		};

		const allProducts = [].concat.apply([], Object.values(newItems));
		const totalPrice = allProducts.reduce((sum, obj) => obj.price + sum, 0)

		return {
			...state,
			items: newItems,
			totalCount: allProducts.length,
			totalPrice: totalPrice
		}
	}

	if (action.type === 'CLEAR_BASKET') {
		return {
			totalPrice: 0,
			totalCount: 0,
			items: {}
		}
	}


	return state;
}

export default basket