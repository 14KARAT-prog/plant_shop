const initialState = {
	items: [],
	isLoading: false
}

const products = (state = initialState, action) => {
	if (action.type === 'SET_PRODUCTS') {
		return {
			...state,
			items: action.payload,
			isLoading: true
		};
	}
	return state;
}

export default products