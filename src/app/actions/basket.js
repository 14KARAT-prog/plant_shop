export const addProductToBasket = (productObj) => ({
	type: 'ADD_PRODUCT_BASKET',
	payload: productObj
})

export const clearBasket = () => ({
	type: 'CLEAR_BASKET',
})
