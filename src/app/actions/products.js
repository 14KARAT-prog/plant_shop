export const fetchProducts = (sortBy, category) => (dispatch) =>  {
	fetch(`http://localhost:3001/products?${category != null ? `types=${category}` : ''}&_sort=${sortBy}`)
		.then((response) => response.json())
		.then(cards => {
			dispatch(setProducts(cards));
		})
}

export const setProducts = (items) => ({
	type: 'SET_PRODUCTS',
	payload: items
});
