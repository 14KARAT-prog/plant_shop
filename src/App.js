import React, {useEffect, useState} from 'react';
import Header from './Components/Header/Header';
import Home from './pages/Home';
import Footer from './Components/Footer/Footer';

import './App.css';
import {Outlet, Route, Routes} from "react-router-dom";
import Basket from "./pages/Basket";
import AboutUs from "./pages/AboutUs";
import Rules from "./pages/Rules";
import ProductsPage from "./pages/ProductsPage";
import ProductPage from "./pages/ProductPage";
import RegistrationPages from "./pages/RegistrationPages";
import SignInPage from "./pages/SignInPage";





function App() {


  const [sliderList, setSliderList] = useState([])

  useEffect(() => {
      fetch('http://localhost:3000/mop/slider.json')
          .then((response) => response.json())
          .then(sliders => {
              setSliderList(sliders.slider)
          })
  }, [])


  return (
        <Routes>
            <Route path={'/'} element={<Layout />}>
                <Route index element={<Home sliders={sliderList} />} exact />
                <Route path={'basket'} element={<Basket />} exact />
                <Route path={'about'} element={<AboutUs />} exact />
                <Route path={'rules'} element={<Rules />} exact />
                <Route path={'registration'} element={<RegistrationPages />} exact />
                <Route path={'signin'} element={<SignInPage />} exact />
                <Route path={'products'} element={<ProductsLayout />}>
                    <Route index element={<ProductsPage />} />
                    <Route path={':title~:price~:urlImage'} element={<ProductPage />} />
                </Route>
            </Route>
        </Routes>
  )
}

function ProductsLayout() {
    return (
        <div className={'main'}>
            <div className={'container'}>
                <div className={'wrapper'}>
                    <h2>Продукт</h2>
                    <Outlet />
                </div>
            </div>
        </div>
    )
}

function Layout() {
    return (
        <div className={'content'}>
            <Header />

            <Outlet />

            <Footer />
        </div>
    )
}

export default App;
