import React from "react";
import './Footer.css';
import {Link} from "react-router-dom";

function Footer() {
	return (
		<section className={'footer'}>
			<div className={'container'}>
				<div className={'footer__content'}>
					<div className={'footer__item'}>
						<h2>Магазин растений</h2>
						<h3>Наше расположение</h3>
						<p>Адрес: Магазины расположены во многих городах России</p>
					</div>
					<div className={'footer__item'}>
						<h3>Компания</h3>
						<Link to={'about'}>
							<p>О нас</p>
						</Link>
						<p>Контакты</p>
						<p>Отзывы</p>
					</div>
					<div className={'footer__item'}>
						<h3>Правила</h3>
						<Link to={'rules'}>
							<p>Условия пользования</p>
						</Link>
						<p>Политика конфиденциальности</p>
					</div>
					<div className={'footer__item'}>
						<h3>Помощь</h3>
						<p>Техническая поддержка</p>
						<p>Обратная связь</p>
					</div>
				</div>
			</div>
		</section>
	)
}

export default Footer