import React, {useEffect, useState} from "react";
import './Registration.css';
import { Link } from 'react-router-dom';

function Registration() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [emailDirty, setEmailDirty] = useState(false);
	const [passwordDirt, setPasswordDirty] = useState(false);
	const [emailError, setEmailError] = useState('Email не может быть пустым');
	const [passwordError, setPasswordError] = useState('Пароль не может быть пустым');
	const [formValid, setFormValid] = useState(false)

	useEffect(() => {
		if (emailError || passwordError) {
			setFormValid(false);
		} else {
			setFormValid(true)
		}
	}, [emailError, passwordError])

	const blurHandler = (event) => {
		switch (event.target.name) {
			case 'email':
				setEmailDirty(true)
				break
			case 'password':
				setPasswordDirty(true)
				break
			default: return
		}
	}

	const passwordHandler = (event) => {
		setPassword(event.target.value)
		if(event.target.value.length < 6 ) {
			setPasswordError('Пароль должен содержать минимум 6 символов')
			if (!event.target.value) {
				setPasswordError('Пароль не может быть пустым')
			}
		} else {
			setPasswordError('')
		}
	}

	const emailHandler = (event) => {
		setEmail(event.target.value)

		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(!re.test(String(event.target.value).toLowerCase())) {
			setEmailError('Некорректный емейл')
		} else {
			setEmailError('')
		}
	}

	const submitForm = () => {
		localStorage.setItem('email', JSON.stringify(email));
		localStorage.setItem('password', JSON.stringify(password));

		setEmail('')
		setPassword('')
	}

	const submitForm1 = (event) => {
		event.preventDefault();
	}

	return (
		<div className={'reg'}>
			<h1>Регистрация</h1>
			<form onSubmit={event => submitForm1(event) }>
				{(emailDirty && emailError) && <div style={{color:'red'}}>{emailError}</div>}
				<input
					value={email}
					onBlur={event => blurHandler(event)}
					name={'email'}
					onChange={event => emailHandler(event) }
					type={'text'}
					placeholder={'Введите email'} />
				{(passwordDirt && passwordError) && <div style={{color:'red'}}>{passwordError}</div>}
				<input
					value={password}
					onBlur={event => blurHandler(event)}
					name={'password'}
					onChange={event => passwordHandler(event) }
					type={'password'}
					placeholder={'Введите пароль'} />
				<Link to={'/'}>
					<button onClick={() => submitForm()} disabled={!formValid} className={'button'} type={'submit'}>Зарегестрироваться</button>
				</Link>
			</form>
		</div>
	)
}

export default Registration