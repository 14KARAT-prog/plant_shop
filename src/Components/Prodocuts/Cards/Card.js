import React from "react";
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";

function Card({ id, title = '', price, urlImage = '', popular, onAddProduct }) {
	return (
		<div className={'products__card-item'}>
				<div className={'products__card__img'}>
					<Link to={`/products/${title}~${price}~${urlImage.replace('https://', '').replaceAll('/', ';')}`}>
						<img src={urlImage} alt={''}/>
					</Link>
				</div>
			<div className={'products__card__text'}>
				<h3>{title}</h3>
				<p>{price}₽</p>
				<p>{popular} - рейтинг популярности</p>

				<button onClick={() => onAddProduct(id, title, price, urlImage)} className={'button'}>В корзину</button>
			</div>
		</div>
	)
}


Card.propTypes = {
	title: PropTypes.string,
	price: PropTypes.number,
	urlImage: PropTypes.string
}

export default Card