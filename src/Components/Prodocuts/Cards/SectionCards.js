import React from 'react';
import PropTypes from 'prop-types';
import './SectionCards.css';
import Card from './Card';

function SectionCards({products = [], onAddProduct}) {
	return (
		<div className={'wrapper'}>
			<div className={'products__cards'}>
				{
					products && products.map((item) => {
						return (
							<Card
								onAddProduct={onAddProduct}
								key = {item.id}
								id={item.id}
								title={item.title}
								price={item.price}
								urlImage={item.urlImage}
								popular = {item.popular}
							/>
						)
					})
				}

			</div>
		</div>
	)
}


SectionCards.propTypes = {
	products: PropTypes.array
}

export default SectionCards