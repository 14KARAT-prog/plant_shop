import React from 'react';
import Sorted from './Sorted';
import Filter from './Filter';

function SectionSortFilter({onClickItem, onClickDrops}) {

	return (
		<div className={'wrapper'}>
			<div className={'products__sort-filter'}>
				<Sorted onClickItem={ onClickItem } items = {[
					'Кактусы',
					'Цветы',
					'Деревья']}
				/>

				<Filter onClickDrops={ onClickDrops } items={[
					{name : 'Популярные', type: 'popular'},
					{name: 'Дешевые', type: 'price&_order=asc'},
					{name : 'Дорогие', type: 'price&_order=desc'},
				]}
				/>
			</div>
		</div>
	)
}

export default SectionSortFilter