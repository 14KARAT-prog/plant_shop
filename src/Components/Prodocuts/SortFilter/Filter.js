import React, {useEffect, useRef, useState} from "react";
import PropTypes from 'prop-types';
import './Filter.css';

function Filter({items, onClickDrops}) {
	const [visibleDrops, setVisibleDrops] = useState(false);
	const [activeDrop, setActiveDrop] = useState(0);
	const filterRef = useRef();

	function handleClick(event) {
		if (!event.path.includes(filterRef.current)) {
			setVisibleDrops(false);
		}
	}

	useEffect(() => {
		document.body.addEventListener('click', handleClick);
	}, [])

	function toggleVisibleDrops() {
		setVisibleDrops(!visibleDrops);
	}

	function onSelectDrop(index, name) {
		setActiveDrop(index);
		setVisibleDrops(false);
		onClickDrops(name);
	}

	return (
		<div ref={filterRef} className={'products__filter'}>
			<span onClick={toggleVisibleDrops}>Фильтр</span>
			<i className="fas fa-caret-down"></i>
			{
				visibleDrops &&
				<div className={'drops'}>
					{
						items && items.map((item, index) => {
							return (
								<p className={activeDrop === index ? 'active-drop' : ''}
								   onClick={() => onSelectDrop(index, item.type)}
									key={`${item.name}_${item.id}`}>
									{item.name}
								</p>
							)
						})
					}
				</div>
			}
		</div>
	)
}


Filter.propTypes = {
	items: PropTypes.array
}

export default Filter