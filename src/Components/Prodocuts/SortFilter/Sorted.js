import React, {useState} from "react";
import PropTypes from 'prop-types';
import './Sorted.css';

function Sorted({items, onClickItem}) {
	const [activeItem, setActiveItem] = useState(null);

	function onSelectItem(index) {
		setActiveItem(index);
		onClickItem(index);
	}

	return (
		<ul className={'products__sorted'}>
			<li className={activeItem === null ? 'active' : ''}
				onClick={() => onSelectItem(null)}>
				<button>Все</button>
			</li>
			{
				items && items.map((item, index) => {
					return (
						<li className={activeItem === index ? 'active' : ''}
							onClick={() => onSelectItem(index)} key={`${item}_${index}`}>
							<button>{item}</button>
						</li>
					)
				})
			}
		</ul>
	)
}


Sorted.propTypes = {
	items: PropTypes.array
}

export default Sorted