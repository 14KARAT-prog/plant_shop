import React, { useState} from "react";
import './SignIn.css';
import { Link } from 'react-router-dom';

function SignIn() {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [handleRef, setHandleRef] = useState('')

	const emailHandler = (event) => {
		setEmail(event.target.value)

	}

	const passwordHandler = (event) => {
	  setPassword(event.target.value)
	}
	const emailLocal = JSON.parse(localStorage.getItem('email'));
	const passwordLocal = JSON.parse(localStorage.getItem('password'));
	const submitForm = () => {
		if (email === emailLocal && password === passwordLocal) {
			setHandleRef('/')
		}
	}


	const submitForm1 = (event) => {
		event.preventDefault();

	}

	return (
		<div className={'reg'}>
			<h1>Введите данные</h1>
			<form onSubmit={event => submitForm1(event) } className={'signIn'}>
				<input
					value={email}
					name={'email'}
					onChange={event => emailHandler(event) }
					type={'text'}
					placeholder={'Введите email'} />
				<input
					value={password}
					name={'password'}
					onChange={event => passwordHandler(event) }
					type={'password'}
					placeholder={'Введите пароль'} />
				<Link to={handleRef}>
					<button onClick={() => submitForm()} className={'button'} type={'submit'}>Войти</button>
				</Link>
			</form>
		</div>
	)
}



export default SignIn