import React from "react";
import PropTypes from 'prop-types';
import './Slider.css';

function Slide({ render = false, photo = '', title = '', text = ''}) {
	function renderSlide() {
		if (render) {
			return (
				<div className={'slider__item'}>
					<div className={'slider__img'}>
						<img src={photo} alt={''}/>
					</div>
					<div className={'slider__text'}>
						<h2>{title}</h2>
						<p>{text}</p>
					</div>
				</div>
			)
		}
	}

	return (
		<div>
			{renderSlide()}
		</div>
	)
}

Slide.propTypes = {
	render: PropTypes.bool.isRequired,
	photo: PropTypes.string,
	title: PropTypes.string,
	text: PropTypes.string
}

export default Slide;

