import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import './Slider.css';
import Slide from './Slide';


function Slider({sliders = []}) {
	let [count, setCount] = useState(0)
	const [images, setImg] = useState([])

	useEffect(() => {
		setImg(sliders)
	}, [sliders])




	function onPrev() {
		setCount(count - 1)
		if (count === 0) {
			setCount(images.length - 1);
		}

		setImg(images.map(item => {
			item.render = item.id === count;
			return item
		}))
	}

	function onNext() {

		setCount(count + 1)
		if (count === images.length - 1) {
			setCount(0);
		}

		setImg(images.map(item => {
			item.render = item.id === count;
			return item
		}))
	}



	return (
		<section className={'slider'}>
			<div className={'container'}>
				<div className={'slider__gallery'}>
					{images.map(item => {
						return (
							<Slide
								key = {item.id}
								render={item.render}
								photo={item.photo}
								title={item.title}
								text={item.text}
							/>
						)
					})}


					<div className={'slider__buttons'}>
						<button onClick={onPrev} id={'#prev'}>Назад</button>
						<button onClick={onNext} id={'next'} className={'button'}>Вперед</button>
					</div>
				</div>
			</div>
		</section>
	)
}

Slider.propTypes = {
	sliders: PropTypes.array
}

export default Slider;


