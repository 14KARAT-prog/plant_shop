import React from "react";
import './BasketCard.css';

function BasketCard({ name = '', price, urlImage}) {
	return (
		<div className={'basket-wrapper'}>
			<div className={'basket__img'}>
				<img src={urlImage} alt={''}/>
			</div>
			<div className={'basket-text'}>
				<p>Название продукта: {name}</p>
				<p>Цена продукта: {price}</p>
			</div>
		</div>
	)
}

export default BasketCard