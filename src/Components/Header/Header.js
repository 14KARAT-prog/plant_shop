import React from 'react';
import '../../index.css';
import './Header.css';
import Logo from '../../img/cactus_logo.svg';
import {Link} from "react-router-dom";
import {useSelector} from 'react-redux';

function Header() {
	const { totalPrice, totalCount } = useSelector(({ basketReducer }) => ({
		totalPrice: basketReducer.totalPrice,
		totalCount: basketReducer.totalCount
	}));

	return (
		<header className={'header'}>
			<div className={'container'}>
				<div className={'header__row'}>

					<Link to={'/'}>
						<div className={'header__logo'}>
							<img src={Logo} alt={'Logo'}/>
						</div>
					</Link>

					<div className={'header__login'}>
						<Link to={'basket'}>
							<span>{totalPrice} ₽</span>
							<i className="fas fa-shopping-basket"></i>
							<span>{totalCount}</span>
						</Link>
						<Link to={'registration'}>
							<button>Sign up</button>
						</Link>
						<Link to={'signin'}>
							<button className={'button'}>Sign in</button>
						</Link>
					</div>

				</div>
			</div>
		</header>
	)
}

export default Header;