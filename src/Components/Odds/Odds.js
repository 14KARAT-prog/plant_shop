import React from "react";
import './Odds.css';
import Odd from './Odd';
import car from "../../img/car.svg";
import headphones from "../../img/headphones.svg";
import flower from "../../img/flower.svg";
import bucks from "../../img/bucks.svg";

function Odds() {
	return (
		<section className={'odds'}>
			<div className={'container'}>
				<div className={'wrapper'}>
					<div className={'wrapper-elements'}>

						<Odd
							image={car}
							title={'Быстрая доставка'}
							text={'Быстрая и своевременная бесплатная доставка'}
						/>
						<Odd
							image={headphones}
							title={'Хорошие отзывы'}
							text={'Покупатели остаются довольные, после наших покупок'}
						/>
						<Odd
							image={flower}
							title={'Оригинальные растения'}
							text={'Только натуральные цветы и растения'}
						/>
						<Odd
							image={bucks}
							title={'Доступные цены'}
							text={'Недорогая продукция, стандартные рыночные цены'}
						/>

					</div>

				</div>
			</div>
		</section>
	)
}

export default Odds