import React from "react";
import PropTypes from 'prop-types';
import './Odds.css';


function Odd( {image = '', title = '', text = ''} ) {
	return (
		<div className={'odds__item'}>
			<div className={'odd__inner-img'}>
				<img src={image} alt={'car'}/>
			</div>
			<span>{title}</span>
			<p>{text}</p>
		</div>
	)
}


Odd.propTypes = {
	image: PropTypes.string,
	title: PropTypes.string,
	text: PropTypes.string
}

export default Odd