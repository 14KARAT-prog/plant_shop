import React from "react";
import BasketCard from '../Components/Basket/BasketCard';
import {useDispatch, useSelector} from 'react-redux';
import { clearBasket } from '../app/actions/basket';
import '../Components/Basket/BasketCard.css';
import basketImg from '../img/basket.jpg';

function Basket() {
	const dispatch = useDispatch()
	const { totalPrice, totalCount, items } = useSelector(({ basketReducer }) => basketReducer)
	const products = Object.keys(items).map(key => {
		return items[key][0]
	})

	const onClearBasket = () => {
		dispatch(clearBasket());
	}


	function basketEmpty() {
		if(!products.length) {
			return (
				<div className={'basket-empty'}>
					Корзина пуста
					<img src={basketImg} alt={''} />
				</div>
			)
		}
	}

	return (
		<div className={'main'}>
			<div className={'container'}>
				<div className={'wrapper'}>
					<h1>Корзина</h1>
					<button onClick={onClearBasket}>Очистить корзину</button>
					{
						products.map((item) => {
							return (
								<BasketCard
									key={item.id}
									name={item.title}
									price={item.price}
									urlImage={item.urlImage}
								/>
							)
						})
					}
					{basketEmpty()}
					<div className={'all-products'}>
						<p>Общая стоимость: {totalPrice} ₽</p>
						<p>Продуктов: {totalCount}</p>
					</div>
					<div className={'Checkout'}>
						<button className={'button'}>Оформить заказ</button>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Basket