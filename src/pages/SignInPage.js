import React from 'react';
import SignIn from '../Components/SignIn/SignIn';


function SignInPage() {
	return (
		<div className={'main'}>
			<div className={'container'}>
				<div className={'wrapper'}>
					<SignIn />
				</div>
			</div>
		</div>
	)
}

export default SignInPage