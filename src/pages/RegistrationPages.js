import React from "react";
import Registration from "../Components/Registration/Registration";


function RegistrationPages() {
	return (
		<div className={'main'}>
			<div className={'container'}>
				<div className={'wrapper'}>
					<Registration />
				</div>
			</div>
		</div>
	)
}

export default RegistrationPages