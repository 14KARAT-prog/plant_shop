import React from "react";
import {useParams} from "react-router-dom";


function ProductPage() {
	const  {title, price, urlImage}  = useParams();
	return (
		<div>
			<h3>Страница продукта - {title}, Цена - {price} ₽ </h3>
			<img src={'https://' + urlImage.replaceAll(';', '/')} alt={''} />
		</div>
	)
}

export default ProductPage