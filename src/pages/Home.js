import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import Slider from '../Components/Slider/Slider';
import Odds from '../Components/Odds/Odds';
import SectionSortFilter from '../Components/Prodocuts/SortFilter/SectionSortFilter';
import SectionCards from '../Components/Prodocuts/Cards/SectionCards';
import relax1 from '../img/relax1.jpg';
import relax2 from '../img/relax2.jpg';

import { useSelector, useDispatch } from 'react-redux';
import {setCategory, setSortBy} from '../app/actions/filter';
import { fetchProducts } from '../app/actions/products';
import { addProductToBasket } from '../app/actions/basket';


function Home({sliders}) {
	const dispatch = useDispatch();

	const addProduct = (id, title, price, urlImage) => {
		const obj = {
			id,
			title,
			price,
			urlImage
		}
		dispatch(addProductToBasket(obj))
	}


	const items  = useSelector(({ productsReducer }) => productsReducer.items);
	const { category, sortBy }  = useSelector(({ filterReducer }) => filterReducer);

	function onSelectCategory(index) {
		dispatch(setCategory(index));
	}

	function onSelectDrops(name) {
		dispatch(setSortBy(name));
	}

	useEffect(() => {
		dispatch(fetchProducts(sortBy, category));
	}, [category, sortBy, dispatch]);

	return (
		<div className={'main'}>
			<Slider sliders={sliders} />
			<Odds />

			<section className={'products'}>
				<div className={'container'}>
					<SectionSortFilter onClickDrops={onSelectDrops} onClickItem={onSelectCategory} />

					<SectionCards onAddProduct={ addProduct } products={items} />
				</div>
			</section>

			<section className={'relax'}>
				<div className={'container'}>
					<div className={'wrapper'}>
						<div className={'relax__inner'}>
							<div className={'relax__inner__left-content'}>
								<div className={'relax__inner-text'}>
									<h2>Купите больше растений, это поможет вам расслабиться</h2>
									<p>Исследования показали, что растения в доме могут снизить напряжение
										у людей почти на 40 %. Причина такого благотворного влияния проста:
										“зелень” напоминает нам о природе на открытом воздухе и более медлительном
										темпе жизни.</p>
								</div>
								<div className={'relax__inner-img'}>
									<img src={relax1} alt={''}/>
								</div>
							</div>
							<div className={'relax__inner__right-content'}>
								<div className={'relax__inner-img'}>
									<img src={relax2} alt={''}/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}


Home.propTypes = {
	sliders: PropTypes.array,
	products: PropTypes.array
}

export default Home